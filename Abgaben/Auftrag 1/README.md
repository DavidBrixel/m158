# Software-Migration

<!-- TOC -->

- [Software-Migration](#software-migration)
  - [Wie ist der Titel Ihrer Software-Migration?](#wie-ist-der-titel-ihrer-software-migration)
  - [Welche Software wird migriert?](#welche-software-wird-migriert)
  - [Hat die Software technische oder produktive Abhängigkeiten? Wenn ja, welche?](#hat-die-software-technische-oder-produktive-abh%C3%A4ngigkeiten-wenn-ja-welche)
  - [Was ist der Grund für die Migration?](#was-ist-der-grund-f%C3%BCr-die-migration)
  - [Darf es eine "Downtime" der Software geben? Wenn ja, wie lange maximal?](#darf-es-eine-downtime-der-software-geben-wenn-ja-wie-lange-maximal)
  - [Welche Kosten entstehen bei einer längeren Downtime als erwartet?](#welche-kosten-entstehen-bei-einer-l%C3%A4ngeren-downtime-als-erwartet)

<!-- /TOC -->

## Wie ist der Titel Ihrer Software-Migration?

Projekt Quantum (Migration des TEST-Systems)

## Welche Software wird migriert?

Software oder Service, der migriert wird: Das TEST-System

## Hat die Software technische oder produktive Abhängigkeiten? Wenn ja, welche?

Technische oder produktive Abhängigkeiten: Ja, das TEST-System hat sowohl technische als auch produktive Abhängigkeiten. Technisch gesehen sind bestimmte Komponenten des Systems mit externen APIs und Datenbanken verbunden. Produktiv gesehen verlassen sich mehrere Geschäftsprozesse auf das TEST-System, um reibungslos zu funktionieren.

## Was ist der Grund für die Migration?

Grund für die Migration: Die bestehende Software ist veraltet, langsam, fehleranfällig und kann den wachsenden Anforderungen der Benutzer nicht mehr gerecht werden. Die Migration zielt darauf ab, das TEST-System zu modernisieren, um eine verbesserte Leistung, Skalierbarkeit und Benutzerfreundlichkeit zu bieten.

## Darf es eine "Downtime" der Software geben? Wenn ja, wie lange maximal?

Downtime der Software: Idealerweise sollte die Downtime während der Migration so gering wie möglich gehalten werden, um die Auswirkungen auf die Benutzer zu minimieren. Die maximale zulässige Downtime wird auf 4 Stunden festgelegt.

## Welche Kosten entstehen bei einer längeren Downtime als erwartet?

Kosten bei längeren Downtimes: Bei einer längeren Downtime als erwartet können verschiedene Kosten entstehen. Dazu gehören potenzieller Umsatzverlust aufgrund nicht verfügbarer Dienste, Reputationsschäden, Entschädigungen für Benutzer oder Kunden, die von der Downtime betroffen sind, und zusätzliche Ressourcenkosten, um die Migration schneller abzuschließen. Die genauen Kosten hängen jedoch von den spezifischen Auswirkungen der Downtime auf das Unternehmen ab.
