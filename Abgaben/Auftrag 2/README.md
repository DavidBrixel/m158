# Auftrag 2

<!-- TOC -->

- [Auftrag 2](#auftrag-2)
  - [Zwei Gründe für eine Software-Migration:](#zwei-gr%C3%BCnde-f%C3%BCr-eine-software-migration)
  - [Migration Definition](#migration-definition)
  - [Software Migrierung im Bereich Software Engineering](#software-migrierung-im-bereich-software-engineering)
  - [Beziehung zwischen Hard- und Software-Migration](#beziehung-zwischen-hard--und-software-migration)
  - [Vermischung mit Software Migrierung](#vermischung-mit-software-migrierung)
  - [Ziel von Reengineering bei Migration](#ziel-von-reengineering-bei-migration)
  - [Reverse Engineering](#reverse-engineering)

<!-- /TOC -->

## Zwei Gründe für eine Software-Migration:

- Technologischer Fortschritt
- Veränderung der Anforderungen

## Migration Definition

- Steht für Umstellungsprozesse im Datenverarbeitungsgebrauch
- Verfahren bei dem ein Teil eines Systems in ein anderes übertragen wird

## Software Migrierung im Bereich Software Engineering

Software Migration gehört zum Bereich der Softwareentwicklung und des Softwaremanagements. Es befasst sich mit dem Prozess des Umzugs oder der Übertragung einer bestehenden Software von einer Plattform, einem System oder einer Umgebung auf eine andere, um ihre Funktionalität und Leistung zu erhalten oder zu verbessern.

## Beziehung zwischen Hard- und Software-Migration

Bei einer Software-Migration kann es erforderlich sein, die zugrunde liegende Hardware-Infrastruktur anzupassen oder zu aktualisieren, um die Software ordnungsgemäß auszuführen. Hard- und Software müssen zusammenarbeiten, um eine erfolgreiche Migration zu gewährleisten.

## Vermischung mit Software Migrierung

Eine Software-Migration sollte niemals mit einer Neuimplementierung oder Neuentwicklung der Software verwechselt werden. Verwechslungen können zu unnötigem Aufwand, Verzögerungen und Qualitätsproblemen führen.

## Ziel von Reengineering bei Migration

Das Reengineering bei einer Migration zielt darauf ab, die bestehende Software während des Umzugs zu optimieren, zu aktualisieren oder zu modernisieren, um sie besser an neue Anforderungen oder Technologien anzupassen.

## Reverse Engineering

Reverse Engineering ist die Analyse einer Software, um ihre Struktur und Funktionalität zu verstehen. Es wird verwendet, um das Verständnis zu verbessern oder Änderungen vorzunehmen, wenn die Dokumentation fehlt, ohne die Software neu entwickeln zu müssen.
